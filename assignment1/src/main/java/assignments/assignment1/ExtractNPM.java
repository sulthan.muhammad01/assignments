package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM 
{
    // Method tahun masuk, kode jurusan, tanggal lahir, penanda sama, dan kode npm untuk mengambil nilai dari suatu variabel
    public static int tahunMasuk(String npmMahasiswa) 
    {
        String masuk = npmMahasiswa.substring(0,2);
        int tahunMasuk = Integer.parseInt(masuk);
        return tahunMasuk;
    }

    public static int kodeJurusan(String npmMahasiswa) 
    {
        String kode = npmMahasiswa.substring(2,4);
        int kodeJurusan = Integer.parseInt(kode);
        return kodeJurusan;
    }

    public static String tanggalLahir(String npmMahasiswa) 
    {
        String tanggalLahir = npmMahasiswa.substring(4,12);
        return tanggalLahir;
    }

    public static int tanda(String npmMahasiswa) 
    {
        String penanda = npmMahasiswa.substring(12,13);
        int kodeTanda = Integer.parseInt(penanda);
        return kodeTanda;
    }

    public static int kodeNPM(String npmMahasiswa) 
    {
        String kode = npmMahasiswa.substring(13,14);
        int kodeNPM = Integer.parseInt(kode);
        return kodeNPM;
    }

    // Method untuk mengalikan index
    public static int kodeNPMMahasiswa(String npmMahasiswa) 
    {
        int sum = 0;
        for(int i = 5; i >= 0; i--) 
        {
            sum += (Character.getNumericValue(npmMahasiswa.charAt(i)) * Character.getNumericValue(npmMahasiswa.charAt(12-i)));
        }
        sum += Character.getNumericValue(npmMahasiswa.charAt(6));

        // Handle apabila hasil penjumlahan lebih dari 10
        while (sum >= 10) 
        {
            String kode = Integer.toString(sum);
            sum = 0;
            for (int i = 0; i < kode.length(); i++) 
            {
                sum += Character.getNumericValue(kode.charAt(i));
            }
        }
        return sum;
    }

    // Method pengecekan panjang NPM, kode jurusan, umur mahasiswa, dan kesesuaian kode NPM
    public static boolean panjangNPM(String npmMahasiswa) 
    {
        if(npmMahasiswa.length() != 14) 
        {
            return false;
        }
        else 
        {
            return true;
        }
    }

    public static boolean umur(String npmMahasiswa) 
    {
        String lahir = tanggalLahir(npmMahasiswa);
        lahir = lahir.substring(4,8);
        int tahunLahir = Integer.parseInt(lahir);
        
        if((2021 - tahunLahir) < 15) 
        {
            return false;
        }
        else 
        {
            return true;
        }
    }

    public static boolean jurusan(String npmMahasiswa) 
    {
        int kodeJurusan = kodeJurusan(npmMahasiswa);
        if(kodeJurusan == 01 || kodeJurusan  == 02 || kodeJurusan == 03 ||
            kodeJurusan == 11 || kodeJurusan == 12) 
        {
            return true;
        }
        else 
        {
            return false;
        }
    }

    public static boolean cekKodeNPM(String npmMahasiswa) 
    {
        if(kodeNPM(npmMahasiswa) != kodeNPMMahasiswa(npmMahasiswa))
        {
            return false;
        }
        else 
        {
            return true;
        }
    }

    // Membuat method validasi NPM
    public static boolean validate(long npmMahasiswa) 
    {
        String npm = Long.toString(npmMahasiswa);
        if(panjangNPM(npm) == true && jurusan(npm) == true && 
           umur(npm) == true && cekKodeNPM(npm) == true)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }

    // Method berisi informasi dari NPM yang sudah divalidasi untuk diekstrak dan dicetak pada main method
    public static String extract(long npmMahasiswa) 
    {
        String npm = Long.toString(npmMahasiswa);
        String jurusan = "";
        int tahunMasuk = tahunMasuk(npm) + 2000;
        if(kodeJurusan(npm) == 01) 
        {
            jurusan = "Ilmu Komputer";
        }
        else if (kodeJurusan(npm) == 02) 
        {
            jurusan = "Sistem Informasi";
        }
        else if (kodeJurusan(npm) == 03) 
        {
            jurusan = "Teknologi Informasi";
        }
        else if (kodeJurusan(npm) == 11) 
        {
            jurusan = "Teknik Telekomunikasi";
        }
        else {
            jurusan = "Teknik Elektro";
        }

        String tanggalLahir = tanggalLahir(npm);
        tanggalLahir = tanggalLahir.substring(0,2)+ "-" + tanggalLahir.substring(2,4)+ "-" + tanggalLahir.substring(4,8);
        return "Tahun masuk: " + tahunMasuk + "\nJurusan: " + jurusan + "\nTanggal Lahir: " + tanggalLahir;
    }

    // Membuat method main untuk menerima input dan mencetak informasi apabila NPM sesuai ketentuan
    public static void main(String args[]) 
    {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while(!exitFlag) {
            System.out.print("Masukkan NPM: ");
            long npmMahasiswa = input.nextLong();
            if (npmMahasiswa < 0) 
            {
                exitFlag = true;
                break;
            }

            if (validate(npmMahasiswa) == true) 
            {
                System.out.println(extract(npmMahasiswa));
                exitFlag = true;
            }
            else 
            {
                System.out.println("NPM tidak valid!");
                exitFlag = true;
            }
        }
        input.close();
    }
}