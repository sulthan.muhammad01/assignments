package assignments.assignment3;

import java.util.Scanner;

public class Main 
{
    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    private static int totalMataKuliah = 0;
    private static int totalElemenFasilkom = 0;

    // Getter untuk mendapatkan elemen fasilkom
    private static ElemenFasilkom getElemenFasilkom(String namaElemenFasilkom) 
    {
        for (int i=0;i<totalElemenFasilkom;i++) 
        {
            if (daftarElemenFasilkom[i].getNama().equals(namaElemenFasilkom)) 
            {
                return daftarElemenFasilkom[i];
            }
        }
        return null;
    }

    // Checker untuk mengetahui apakah elemen fasilkom merupakan mahasiswa, dosen, atau elemen kantin 
    private static boolean checkMahasiswa(String namaMahasiswa) 
    {
        if (getElemenFasilkom(namaMahasiswa).getTipe().equals("Mahasiswa"))
        {
            return true;
        }
        return false;
    }

    private static boolean checkDosen(String namaDosen) 
    {
        if (getElemenFasilkom(namaDosen).getTipe().equals("Dosen"))
        {
            return true;
        }
        return false;
    }

    private static boolean checkElemenKantin(String namaElemenKantin) 
    {
        if (getElemenFasilkom(namaElemenKantin).getTipe().equals("ElemenKantin"))
        {
            return true;
        }
        return false;
    }

    // Getter untuk mendapatkan mata kuliah
    private static MataKuliah getMataKuliah(String nama) 
    {
        for (int i = 0; i < totalMataKuliah; i++) 
        {
            if (daftarMataKuliah[i].getNama().equals(nama)) 
            {
                return daftarMataKuliah[i];
            }
        }
        return null;
    }

    // Method untuk menambahkan mahasiswa pada program
    private static void addMahasiswa(String nama, long npm) 
    {
        daftarElemenFasilkom[totalElemenFasilkom++] = new Mahasiswa(nama, npm);
        System.out.println(String.format("%s berhasil ditambahkan", nama));
    }
                
    // Method untuk menambahkan dosen pada program
    private static void addDosen(String nama) 
    {
        daftarElemenFasilkom[totalElemenFasilkom++] = new Dosen(nama);
        System.out.println(String.format("%s berhasil ditambahkan", nama));
    }

    // Method untuk menambahkan elemen kantin pada program
    private static void addElemenKantin(String nama) 
    {
        daftarElemenFasilkom[totalElemenFasilkom++] = new ElemenKantin(nama);
        System.out.println(String.format("%s berhasil ditambahkan", nama));
    }

    // Method untuk saling menyapa
    private static void menyapa(String objek1, String objek2) 
    {
        ElemenFasilkom elemen1 = getElemenFasilkom(objek1);
        ElemenFasilkom elemen2 = getElemenFasilkom(objek2);
        if (!objek1.equals(objek2)
        && elemen1!=null
        && elemen2!=null)
        {
            elemen1.menyapa(elemen2);
        }
        else
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
    }

    // Method untuk menambahkan suatu makanan untuk dijual oleh seorang elemen kantin
    private static void addMakanan(String objek, String namaMakanan, long harga) 
    {
        ElemenFasilkom elemenKantin = getElemenFasilkom(objek);
        if (elemenKantin!=null
        && checkElemenKantin(objek))
        {
            ((ElemenKantin)elemenKantin).setMakanan(namaMakanan, harga);
        }
        else
            System.out.println(String.format("[DITOLAK] %s bukan merupakan elemen kantin", objek));
    }

    // Method untuk membeli makanan dari seorang elemen kantin
    private static void membeliMakanan(String objek1, String objek2, String namaMakanan) 
    {
        ElemenFasilkom pembeli = getElemenFasilkom(objek1);
        ElemenFasilkom penjual = getElemenFasilkom(objek2);
        if (objek1.equals(objek2))
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
        else if (pembeli!=null
        && penjual!=null 
        && checkElemenKantin(objek2))
        {
            pembeli.membeliMakanan(pembeli,penjual,namaMakanan);
        }
        else
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
    }

    // Method untuk membuat suatu mata kuliah
    private static void createMatkul(String nama, int kapasitas) 
    {
        daftarMataKuliah[totalMataKuliah++] = new MataKuliah(nama, kapasitas);
        System.out.println(String.format("%s berhasil ditambahkan dengan kapasitas %d", nama, kapasitas));
    }

    // Method untuk menambahkan suatu mata kuliah pada daftar mata kuliah seorang mahasiswa
    private static void addMatkul(String objek, String namaMataKuliah) 
    {
        ElemenFasilkom mahasiswa = getElemenFasilkom(objek);
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        if (mahasiswa!=null
        && mataKuliah!=null
        && checkMahasiswa(objek))
        {
            ((Mahasiswa)mahasiswa).addMatkul(mataKuliah);
        }
        else
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
    }

    // Method untuk menghapus suatu mata kuliah pada daftar mata kuliah seorang mahasiswa
    private static void dropMatkul(String objek, String namaMataKuliah) 
    {
        ElemenFasilkom mahasiswa = getElemenFasilkom(objek);
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        if (mahasiswa!=null
        && mataKuliah!=null
        && checkMahasiswa(objek))
        {
            ((Mahasiswa)mahasiswa).dropMatkul(mataKuliah);
        }
        else 
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
    }

    // Method untuk menempatkan dosen untuk mengajar suatu mata kuliah
    private static void mengajarMatkul(String objek, String namaMataKuliah) 
    {
        ElemenFasilkom dosen = getElemenFasilkom(objek);
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        if (dosen!=null
        && mataKuliah!=null
        && checkDosen(objek))
        {
            ((Dosen)dosen).mengajarMataKuliah(mataKuliah);
        }
        else
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
    }

    // Method untuk memberhentikan dosen dalam mengajar suatu mata kuliah
    private static void berhentiMengajar(String objek) 
    {
        ElemenFasilkom dosen = getElemenFasilkom(objek);
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        if (dosen!=null
        && mataKuliah!=null
        && checkDosen(objek))
        {
            ((Dosen)dosen).dropMataKuliah();
        }
        else
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
    }

    // Method untuk memberikan ringkasan seorang mahasiswa
    private static void ringkasanMahasiswa(String objek) 
    {
        ElemenFasilkom elemenFasilkom = getElemenFasilkom(objek);
        if (mahasiswa!=null
        && checkMahasiswa(objek))
        {
            Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom;
            System.out.println("Nama: " + mahasiswa);
            System.out.println("Tanggal Lahir: " + mahasiswa.getTanggalLahir());
            System.out.println("Jurusan: " + mahasiswa.getJurusan());
            System.out.println("Daftar Mata Kuliah: ");
            if (mahasiswa.getJumlahMatkul() != 0) 
                {
                    int counter = 1;
                    for (int i = 0; i < mahasiswa.getJumlahMatkul(); i++) 
                {
                    if (mahasiswa.getDaftarMatkul()[i] == null) 
                {
                    continue;
                }
                System.out.println(String.format("%d. %s\n", counter, mahasiswa.getDaftarMatkul()[i]));
                counter++;
                }
            }
            /*
            Handle apabila belum mengambil mata kuliah
            */
            else 
                System.out.println("Belum ada mata kuliah yang diambil");
        }
        /*
            Handle elemen fasilkom bukan mahasiswa
        */
        else
            System.out.println(String.format("[DITOLAK] %s bukan merupakan seorang mahasiswa", objek));
    }

    // Method untuk memberikan ringkasan suatu mata kuliah
    private static void ringkasanMataKuliah(String namaMataKuliah) 
    {
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        if (mataKuliah!=null)
        {
            System.out.println("Nama mata kuliah: " + mataKuliah.getNama());
            System.out.println("Jumlah mahasiswa: " + mataKuliah.getSumOfMahasiswa());
            System.out.println("Kapasitas: " + mataKuliah.getKapasitas());
            System.out.println("Dosen pengajar: " + mataKuliah.getDosen());
            System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");
        }
        /*
        Handle apabila tidak ada mahasiswa
        */
        if (mataKuliah.getSumOfMahasiswa() == 0) 
        {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
            return;
        }
        /*
        Mencetak daftar mahasiswa yang mengambil suatu mata kuliah sesuai format
        */
        else
        {
            for (int i = 0; i < mataKuliah.getSumOfMahasiswa(); i++) 
            {
                System.out.println(String.format("%d. %s\n", i+1, mataKuliah.getDaftarMahasiswa()[i]));
            }
        }
    }

    // Method untuk melakukan kalkulasi mengenai tingkat friendship setiap orang
    private static void nextDay() 
    {
        int jumlah = ((totalElemenFasilkom-1)/2);
        for (int i = 0; i < totalElemenFasilkom; i++) 
        {
            if (daftarElemenFasilkom[i].getTelahSapa() >= jumlah) 
            {
                daftarElemenFasilkom[i].setFriendship(10);
            }
            else 
            {
                daftarElemenFasilkom[i].setFriendship(-5);
            }
            daftarElemenFasilkom[i].resetMenyapa();
        }
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
    }

    // Method untuk memberikan informasi mengenai tingkat friendship setiap orang
    private static void friendshipRanking() 
    {
        ElemenFasilkom[] sortedFriendship = java.util.Arrays.copyOfRange(daftarElemenFasilkom, 0, totalElemenFasilkom);
        java.util.Arrays.sort(sortedFriendship);
        for (int i=0;i<totalElemenFasilkom;i++)
        {
            System.out.println(String.format("%d. %s(%d)", i+1, sortedFriendship[i], sortedFriendship[i].getFriendship()));
        }
    }

    // Method ketika akan memberhentikan program
    private static void programEnd() 
    {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :\n");
        friendshipRanking();
    }

    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);
        
        while (true) 
        {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) 
            {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } 
            else if (in.split(" ")[0].equals("ADD_DOSEN")) 
            {
                addDosen(in.split(" ")[1]);
            } 
            else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) 
            {
                addElemenKantin(in.split(" ")[1]);
            } 
            else if (in.split(" ")[0].equals("MENYAPA")) 
            {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } 
            else if (in.split(" ")[0].equals("ADD_MAKANAN")) 
            {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } 
            else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) 
            {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } 
            else if (in.split(" ")[0].equals("CREATE_MATKUL")) 
            {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } 
            else if (in.split(" ")[0].equals("ADD_MATKUL")) 
            {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } 
            else if (in.split(" ")[0].equals("DROP_MATKUL")) 
            {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } 
            else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) 
            {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } 
            else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) 
            {
                berhentiMengajar(in.split(" ")[1]);
            } 
            else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) 
            {
                ringkasanMahasiswa(in.split(" ")[1]);
            } 
            else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) 
            {
                ringkasanMataKuliah(in.split(" ")[1]);
            } 
            else if (in.split(" ")[0].equals("NEXT_DAY")) 
            {
                nextDay();
            } 
            else if (in.split(" ")[0].equals("PROGRAM_END")) 
            {
                programEnd();
                break;
            }
        }
    }
}