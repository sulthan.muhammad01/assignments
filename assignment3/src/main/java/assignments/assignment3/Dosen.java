package assignments.assignment3;

public class Dosen extends ElemenFasilkom 
{
    private MataKuliah mataKuliah;

    /* Constructor */
    public Dosen(String nama) 
    {
        super(nama, "Dosen");
    }

    /* Getter */
    public MataKuliah getMataKuliah() 
    {
        return this.mataKuliah;
    }

    /* 
        Method untuk menempatkan dosen pada suatu mata kuliah 
    */
    public void mengajarMataKuliah(MataKuliah mataKuliah) 
    {
        if (mataKuliah.getDosen()==null
        && this.mataKuliah==null)
        {
            this.mataKuliah.addDosen(this);
            System.out.println(String.format("%s mengajar mata kuliah %s", this.getNama(), this.mataKuliah));
        }
        else if (this.mataKuliah!=null)
        {
            System.out.println(String.format("[DITOLAK] %s sudah mengajar mata kuliah %s", mataKuliah.getDosen(), mataKuliah.getNama()));
        }
        else if (mataKuliah.getDosen()!=null)
        {
            System.out.println(String.format("[DITOLAK] %s sudah memiliki dosen pengajar", mataKuliah.getNama()));
        } 
    }

    /* 
        Method untuk men-drop dosen pada suatu mata kuliah 
    */
    public void dropMataKuliah() 
    {
        if (this.mataKuliah != null) 
        {
            this.mataKuliah.dropDosen();
            this.mataKuliah = null;
            System.out.println(String.format("%s berhenti mengajar %s", this.getNama(), this.mataKuliah));
        }
        System.out.println(String.format("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun", this.getNama()));
    }
}