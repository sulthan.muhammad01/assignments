package assignments.assignment3;

public class Mahasiswa extends ElemenFasilkom 
{
    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    private long npm;
    private String tanggalLahir;
    private String jurusan;

    /* Constructor */
    public Mahasiswa(String nama, long npm) 
    {
        super(nama, "Mahasiswa");
        this.npm = npm;
        tanggalLahir = extractTanggalLahir(npm);
        jurusan = extractJurusan(npm);
    }

    /* Getter dan Setter */
    public MataKuliah[] getDaftarMatkul() 
    {
        return this.daftarMataKuliah;
    }

    public long getNPM() 
    {
        return this.npm;
    }

    public String getTanggalLahir() 
    {
        return this.tanggalLahir;
    }

    public String getJurusan() 
    {
        return this.jurusan;
    }

    public int getJumlahMatkul() 
    {
        return this.getJumlahMatkul();
    }

    /* 
        Method untuk mengecek status suatu mata kuliah  
    */
    public boolean checkStatus(MataKuliah mataKuliah)
    {
        for (int i=0;i<10;i++)
        {
            if (daftarMataKuliah[i].getNama().equals(mataKuliah.getNama()))
            {
                return true;
            }
        }
        return false;
    }

    /* 
        Method untuk mengecek status suatu mata kuliah  
    */
    public boolean checkMatkul(MataKuliah namaMataKuliah)
    {
        for (int i=0; i<10; i++) 
        {
            if (this.daftarMataKuliah[i] == namaMataKuliah)
                return false;
        }
        return true;
    }   

    /* 
        Method untuk meng-add mata kuliah 
    */
    public void addMataKuliah(MataKuliah namaMataKuliah)
    {
        for (int i=0; i<10; i++) 
        {
            if (this.daftarMataKuliah[i] == null)
            {
                this.daftarMataKuliah[i] = namaMataKuliah;
                System.out.println(String.format("%s berhasil menambahkan matakuliah %s", this.getNama(), namaMataKuliah.toString()));
                return;
            }
        }
    }

    /* 
        Method untuk men-remove mata kuliah  
    */
    public void removeMataKuliah(MataKuliah namaMataKuliah)
    {
        for (int i=0; i<10; i++) 
        {
            if (this.daftarMataKuliah[i] != null
            && this.daftarMataKuliah[i]==namaMataKuliah)
            {
                this.daftarMataKuliah[i] = null;
                System.out.println(String.format("%s berhasil drop mata kuliah %s", this.getNama(), namaMataKuliah.toString()));
                return;
            }
        }
    }

    /* 
        Method untuk meng-add mata kuliah 
    */
    public void addMatkul(MataKuliah mataKuliah) 
    {
        if (checkMatkul(mataKuliah)
        &&(mataKuliah.getSumOfMahasiswa()+1 <= mataKuliah.getKapasitas())) 
        {
            mataKuliah.addMahasiswa(this);
            addMataKuliah(mataKuliah);
        }
        else if (mataKuliah.getSumOfMahasiswa()+1 > mataKuliah.getKapasitas())
        {
            System.out.println(String.format("[DITOLAK] %s telah penuh kapasitasnya", mataKuliah.getNama()));
        }
        else
        {
            System.out.println(String.format("[DITOLAK] %s telah diambil sebelumnya", mataKuliah.getNama()));
        } 
    }

    /* 
        Method untuk men-remove mata kuliah  
    */
    public void dropMatkul(MataKuliah mataKuliah) 
    {
        if (checkMatkul(mataKuliah))
            System.out.println(String.format("[DITOLAK] %s belum pernah diambil", mataKuliah.getNama()));
        else
        {
            removeMataKuliah(mataKuliah);
            mataKuliah.dropMahasiswa(this);
        }
    }

    /* 
        Method untuk mengambil informasi tanggal lahir seseorang dari npmnya
    */
    public String extractTanggalLahir(long npm) 
    {
        String hari = ("" + npm).substring(4,6);
        String bulan = ("" + npm).substring(6,8);
        String tahun = ("" + npm).substring(8,12);
        return Integer.parseInt(hari) + "-" + Integer.parseInt(bulan) + "-" + Integer.parseInt(tahun);
    }

    public String extractJurusan(long npm) 
    {
        String kode = ("" + npm).substring(2,4);
        if (kode.equals("01"))
            return "Ilmu Komputer";
        else
            return "Sistem Informasi";
    }
}