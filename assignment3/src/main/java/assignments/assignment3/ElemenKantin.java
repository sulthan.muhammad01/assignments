package assignments.assignment3;

public class ElemenKantin extends ElemenFasilkom 
{
    private Makanan[] daftarMakanan = new Makanan[10];
    private int sumOfMakanan;

    /* Constructor */
    public ElemenKantin(String nama) 
    {
        super(nama, "ElemenKantin");
    }

    /* Getter */
    public int getSumOfMakanan()
    {
        return this.sumOfMakanan;
    }

    public Makanan[] getDaftarMakanan()
    {
        return this.daftarMakanan;
    }

    public Makanan getMakanan(String nama)
    {
        Makanan makanan = null;
        for (int i=0; i < this.sumOfMakanan; i++)
        {
            if(daftarMakanan[i]!= null
            && daftarMakanan[i].toString().equals(nama))  
            {
                makanan = daftarMakanan[i];
                break;
            }
        }
        return makanan;
    }

    /* 
        Method untuk mendaftarkan suatu makanan dan harganya 
    */
    public void setMakanan(String nama, long harga) 
    {
        Makanan makanan = getMakanan(nama);
        if(makanan == null)
        {
            this.daftarMakanan[this.sumOfMakanan] = new Makanan(nama, harga);
            this.sumOfMakanan++;
            System.out.println(String.format("%s telah mendaftarkan makanan %s dengan harga %d", this.getNama(), nama, harga));
        }
        else
            System.out.println(String.format("[DITOLAK] %s sudah pernah terdaftar", nama));
    }
}