package assignments.assignment3;

class MataKuliah 
{
    private String nama;
    private int kapasitas;
    private Dosen dosen;
    private Mahasiswa[] daftarMahasiswa;
    private int sumOfMahasiswa;

    /* Constructor */
    public MataKuliah(String nama, int kapasitas)
    {
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    /* Getter */
    public String getNama()
    {
        return this.nama;
    }

    public int getKapasitas()
    {
        return this.kapasitas;
    }

    public int getSumOfMahasiswa()
    {
        return this.sumOfMahasiswa;
    }

    public String getDosen()
    {
        if (this.dosen==null) 
            return "Belum ada";
        else
            return this.dosen.getNama();
    }

    public Mahasiswa[] getDaftarMahasiswa()
    {
        return this.daftarMahasiswa;
    }

    /* 
        Method untuk menambahkan seorang mahasiswa pada suatu mata kuliah 
    */
    public void addMahasiswa(Mahasiswa mahasiswa) 
    {
        for (int i=0; i<daftarMahasiswa.length; i++) 
        {
            if (this.daftarMahasiswa[i] == null)
            {
                this.daftarMahasiswa[i] = mahasiswa;
                this.sumOfMahasiswa++;
                return;
            }
        }
    }

    /* 
        Method untuk menghapus seorang mahasiswa pada suatu mata kuliah
    */
    public void dropMahasiswa(Mahasiswa mahasiswa) 
    {
        for (int i = 0; i<daftarMahasiswa.length; i++){
            if (this.daftarMahasiswa[i].toString().equals(mahasiswa.getNama()))
            {
                this.daftarMahasiswa[i] = null;
                this.sumOfMahasiswa--;
                return;
            }
        }

    }

    /* 
        Method untuk men-add dosen
    */
    public void addDosen(Dosen dosen) 
    {
        this.dosen = dosen;
    }

    /* 
        Method untuk men-drop dosen
    */
    public void dropDosen() 
    {
        this.dosen = null;
    }

    public String toString() 
    {
        return this.nama;
    }
}