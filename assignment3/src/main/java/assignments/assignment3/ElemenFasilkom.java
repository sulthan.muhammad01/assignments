package assignments.assignment3;

public abstract class ElemenFasilkom 
{
    private String tipe;
    private String nama;
    private int friendship;
    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];
    private int telahSapa;

    /* Constructor */
    protected ElemenFasilkom(String nama, String tipe)
    {
        this.nama = nama;
        this.tipe = tipe;
    }

    /* Getter dan Setter */
    public String getNama()
    {
        return this.nama;
    }

    public String getTipe()
    {
        return this.tipe;
    }

    public int getFriendship()
    {
        return this.friendship;
    }

    public int getTelahSapa()
    {
        return this.telahSapa;
    }

    public void setFriendship(int friendship)
    {
        this.friendship = friendship;
    }

    /* 
        Method checkSameClass
        Mengecek apakah elemen yang saling menyapa merupakan dosen dan mahasiswa berada pada matakuliah yang sama
    */
    public boolean checkSameClass(ElemenFasilkom elemenFasilkom) {
        if (this.getTipe().equals("Mahasiswa") 
        && elemenFasilkom.getTipe().equals("Dosen") 
        && ((Mahasiswa)this).checkStatus(((Dosen)elemenFasilkom).getMataKuliah()) 
        && ((Dosen)elemenFasilkom).getMataKuliah() != null) 
        {
            return true;
        }
        else if (this.getTipe().equals("Dosen") 
        && elemenFasilkom.getTipe().equals("Mahasiswa") 
        && ((Mahasiswa)elemenFasilkom).checkStatus(((Dosen)this).getMataKuliah()) 
        && ((Dosen)this).getMataKuliah() != null)
        { 
           return true;
        }
        return false;
    }

    /* 
        Method menyapa elemen
    */
    public void menyapa(ElemenFasilkom elemenFasilkom) 
    {
        boolean belumMenyapa = false;
        for (int i=0; i<this.telahSapa; i++) 
        {
            for (int j=0; j<elemenFasilkom.telahSapa; j++)
            {
                if (this.telahMenyapa[i]==elemenFasilkom
                && elemenFasilkom.telahMenyapa[j]==this)
                {
                    belumMenyapa = true;
                    System.out.println(String.format("[DITOLAK] %s telah menyapa %s hari ini", this.nama, elemenFasilkom.getNama()));
                    return;
                }
            }
        }
        if (!belumMenyapa)
        {
            this.telahMenyapa[this.telahSapa]=elemenFasilkom;
            elemenFasilkom.telahMenyapa[elemenFasilkom.telahSapa]=this;
            this.telahSapa++;
            elemenFasilkom.telahSapa++;
            if (checkSameClass(elemenFasilkom)) 
            {
                this.setFriendship(2);
                elemenFasilkom.setFriendship(2);
            }
            System.out.println(String.format("%s menyapa dengan %s", this.nama, elemenFasilkom.getNama()));
        }
    }

    /* 
        Method untuk mereset list orang yang sudah disapa
    */
    public void resetMenyapa() 
    {
        for (int i = 0; i < telahMenyapa.length; i++) 
        {
            telahMenyapa[i] = null;
        }
    }

    /* 
        Method untuk membeli makanan dari elemen kantin
    */
    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) 
    {
        if (penjual instanceof ElemenKantin)
        {
            ElemenKantin elemenKantin = (ElemenKantin) penjual;
            for (int i=0; i<elemenKantin.getSumOfMakanan(); i++)
            {
                if (elemenKantin.getDaftarMakanan()[i].getNama().equals(namaMakanan))
                {
                    pembeli.setFriendship(1);
                    penjual.setFriendship(1);
                    System.out.println(String.format("%s berhasil membeli %s seharga %d", pembeli.toString(), namaMakanan, elemenKantin.getDaftarMakanan()[i].getHarga()));
                }
                else
                    System.out.println(String.format("[DITOLAK] %s tidak menjual %s", elemenKantin.toString(), namaMakanan));
            }
        }
    }

    public String toString()
    {
        return this.nama;
    } 
}