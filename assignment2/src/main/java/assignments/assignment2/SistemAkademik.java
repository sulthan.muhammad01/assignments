package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);

    // Getter untuk mengambil salah satu mahasiswa dari array daftar mahasiswa
    private Mahasiswa getMahasiswa(long npm) 
    {
        for (Mahasiswa mahasiswa : daftarMahasiswa) 
        {
            if (mahasiswa.getNPM() == npm 
                && mahasiswa != null) 
            {
                return mahasiswa;
            }
        }
        return null;
    }

    // Getter untuk mengambil salah satu mata kuliah dari array daftar mata kuliah
    private MataKuliah getMataKuliah(String namaMataKuliah) 
    {
        for (MataKuliah mataKuliah : daftarMataKuliah) 
        {
            if (mataKuliah.getNama().equals(namaMataKuliah) 
                && mataKuliah != null) 
            {
                return mataKuliah;
            }
        }
        return null;
    }

    // Method tambah mata kuliah pada sistem akademik mahasiswa dan dicetak sesuai format
    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);

        // Menerima input user berupa jumlah matkul dan nama matkul yang ingin ditambah
        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");
        for(int i = 0; i < banyakMatkul; i++)
        {
            System.out.print("Nama matakuliah " + i + 1 + " : ");
            String namaMataKuliah = input.nextLine();

            // Handle jumlah matkul agar tidak melebihi kapasitas
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            if (mahasiswa.getMataKuliah(mataKuliah.toString()) == null) 
            {
                if (mataKuliah.getKapasitas() > mataKuliah.getSumOfMahasiswa()) 
                {
                    if (mahasiswa.getSumOfMatkul() < 10) 
                    {
                        mahasiswa.addMatkul(mataKuliah);
                        mataKuliah.addMahasiswa(mahasiswa);
                    }
                    else 
                    {
                        System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
                        break;
                    }
                }

                // Handle jika kapasitasnya penuh
                else 
                {
                    System.out.printf("[DITOLAK] %s telah penuh kapasitasnya.\n", mataKuliah.getNama());
                    break;
                }
            }

            // Handle jika matkul sudah pernah diambil sebelumnya
            else 
            {
                System.out.printf("[DITOLAK] %s telah diambil sebelumnya.\n", mataKuliah.getNama());
                break;
            } 
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    // Method drop mata kuliah pada sistem akademik mahasiswa dan dicetak sesuai format
    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);

        // Handle apabila belum ada mata kuliah yang diambil
        if (mahasiswa.getSumOfMatkul() == 0) 
        {
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
            return;
        }

        // Menerima input user berupa jumlah matkul dan nama matkul yang ingin di-drop
        System.out.print("Banyaknya Matkul yang Di-drop: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang di-drop:");
        for(int i = 0; i < banyakMatkul; i++)
        {
            System.out.print("Nama matakuliah " + i + 1 + " : ");
            String namaMataKuliah = input.nextLine();

            // Handle apabila matkul yang ingin di-drop belum pernah di-add ke dalam sistem
            MataKuliah mataKuliah = mahasiswa.getMataKuliah(namaMataKuliah);
            if (mataKuliah == null) 
            {
                System.out.printf("[DITOLAK] %s belum pernah diambil.\n", getMataKuliah(namaMataKuliah));
                break;
            }
            mahasiswa.dropMatkul(mataKuliah);
            mataKuliah.dropMahasiswa(mahasiswa);
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    // Method untuk mencetak ringkasan akademis
    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);

        // Mencetak output sesuai format
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + mahasiswa);
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + mahasiswa.getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

        // Handle apabila belum ada mata kuliah yang diambil
        if (mahasiswa.getSumOfMatkul() == 0) 
        {
            System.out.println("Belum ada mata kuliah yang diambil");
        }

        // Mencetak jumlah mata kuliah sesuai format di hasil cek IRS
        MataKuliah[] daftarMataKuliah = mahasiswa.getMatkul();
        int counter = 0;
        for (int i = 0; i < daftarMataKuliah.length; i++) 
        {
            if (daftarMataKuliah[i] != null) 
            {
                counter++;
                System.out.println(counter + ". " + daftarMataKuliah[i]);
            }
        }

        mahasiswa.cekIRS();
        System.out.println("Total SKS: " + mahasiswa.getTotalSKS());
        
        System.out.println("Hasil Pengecekan IRS:");

        // Mengecek apakah IRS bermasalah atau tidak 
        String[] masalahIRS = mahasiswa.getMasalahIRS();
        if (masalahIRS[0] == null) 
        {
            System.out.println("IRS tidak bermasalah.");
        }

        // Mencetak jumlah masalah IRS sesuai format di hasil cek IRS
        else 
        {
            for (int i = 0; i < masalahIRS.length; i++) 
            {
                if (masalahIRS[i] != null) 
                {
                    System.out.println(i + 1 +". " + masalahIRS[i]);
                }
            }
        }
    }

    // Method untuk mencetak ringkasan matkul
    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        
        // Mencetak output sesuai format
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + mataKuliah.getNama());
        System.out.println("Kode: " + mataKuliah.getKode());
        System.out.println("SKS: " + mataKuliah.getSKS());
        System.out.println("Jumlah mahasiswa: " + mataKuliah.getSumOfMahasiswa());
        System.out.println("Kapasitas: " + mataKuliah.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");

        // Handle apabila matkul belum pernah diambil mahasiswa
        if (mataKuliah.getSumOfMahasiswa() == 0) 
        {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
            return;
        }
        // Mencetak daftar mahasiswa yang mengambil suatu mata kuliah sesuai format di hasil cek IRS
        Mahasiswa[] daftarMahasiswa = mataKuliah.getDaftarMahasiswa();
        int counter = 0;
        for (int i = 0; i < daftarMahasiswa.length; i++) 
        {
            if (daftarMahasiswa[i] != null) 
            {
                counter++;
                System.out.println(counter + ". " + daftarMahasiswa[i]);
            }
        }
    }

    // Method untuk mencetak daftar menu pada program dan memanggil method sesuai perintah
    private void daftarMenu()
    {
        int pilihan = 0;
        boolean exit = false;
        while (!exit) 
        {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try 
            {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) 
            {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) 
            {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) 
            {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) 
            {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) 
            {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) 
            {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }
    }

    //Method untuk mencetak format program saat pertama kali dijalankan
    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i = 0; i < banyakMatkul; i++)
        {
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            
            // Instance mata kuliah untuk dimasukkan ke array
            daftarMataKuliah[i] = new MataKuliah(dataMatkul[0], dataMatkul[1], sks, kapasitas);
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i = 0; i < banyakMahasiswa; i++)
        {
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);

            // Instance mahasiswa untuk dimasukkan ke array
            daftarMahasiswa[i] = new Mahasiswa(dataMahasiswa[0], npm);
        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) 
    {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }
}
