package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int sumOfMahasiswa;

    // Constructor mata kuliah
    public MataKuliah(String kode, String nama, int sks, int kapasitas)
    {
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    // Method getter untuk mengambil kode, nama, sks, kapasitas, dan daftar mahasiswa
    public String getKode()
    {
        return this.kode;
    }

    public String getNama()
    {
        return this.nama;
    }

    public int getSKS()
    {
        return this.sks;
    }

    public int getKapasitas()
    {
        return this.kapasitas;
    }    

    public Mahasiswa[] getDaftarMahasiswa()
    {
        return this.daftarMahasiswa;
    }

    public int getSumOfMahasiswa()
    {
        return this.sumOfMahasiswa;
    }

    // Method menambah mahasiswa
    public void addMahasiswa(Mahasiswa mahasiswa) 
    {
        for (int i = 0; i < this.kapasitas; i++) 
        {
            if (this.daftarMahasiswa[i] == null) 
            {
                this.daftarMahasiswa[i] = mahasiswa;
                this.sumOfMahasiswa++;
                return;
            }
        }
        System.out.println("[DITOLAK] OS telah penuh kapasitasnya.");
    }

    // Method drop mahasiswa
    public void dropMahasiswa(Mahasiswa mahasiswa) 
    {
        for (int i = 0; i < this.kapasitas; i++) 
        {
            if (this.daftarMahasiswa[i] == mahasiswa) 
            {
                this.daftarMahasiswa[i] = null;
                sumOfMahasiswa--;
                return;
            }
        }
        System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
    }

    public String toString() 
    {
        return this.nama;
    }
}
