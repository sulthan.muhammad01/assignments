package assignments.assignment2;

public class Mahasiswa 
{
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;
    private int sumOfMatkul;
    private static final int maxMatkul = 10;

    // Constructor mahasiswa  
    public Mahasiswa(String nama, long npm)
    {
        this.nama = nama;
        this.npm = npm;
        String kode = Long.toString(npm).substring(2,4);
        if (kode.equals("01")) 
        {
            this.jurusan = "Ilmu Komputer";
        }
        else if(kode.equals("02"))  
        {
            this.jurusan = "Sistem Informasi";
        }
    }

    // Method getter untuk mengambil nama, jurusan, NPM, masalah IRS, Matkul, total sks, dan sum of matkul
    public String getNama()
    {
        return this.nama;
    }

    public String getJurusan()
    {
        return this.jurusan;
    }

    public long getNPM()
    {
        return this.npm;
    }

    public String[] getMasalahIRS() 
    {
        return this.masalahIRS;
    }

    public MataKuliah[] getMatkul() 
    {
        return this.mataKuliah;
    }

    public int getTotalSKS() 
    {
        return this.totalSKS;
    }

    public int getSumOfMatkul() 
    {
        return this.sumOfMatkul;
    }

    
    // Method untuk add mata kuliah
    public void addMatkul(MataKuliah addMataKuliah)
    {
        for (int i = 0; i < maxMatkul; i++) 
        {
            if (this.mataKuliah[i] == null) 
            {
                this.mataKuliah[i] = addMataKuliah;
                this.totalSKS += addMataKuliah.getSKS();
                sumOfMatkul++;
                return;
            }
        }
    }

    // Method untuk drop mata kuliah 
    public void dropMatkul(MataKuliah dropMataKuliah){
        for (int i = 0; i < maxMatkul; i++) 
        {
            if (this.mataKuliah[i] == dropMataKuliah) 
            {
                this.mataKuliah[i] = null;
                this.totalSKS -= dropMataKuliah.getSKS();
                sumOfMatkul--;
                return;
            }
        }
    }

    public MataKuliah getMataKuliah(String namaMatkul) 
    {
        for (int i = 0; i < 10; i++) 
        {
            if (mataKuliah[i] != null 
                && mataKuliah[i].getNama().equals(namaMatkul)) 
            {
                return mataKuliah[i];
            }
            else 
            {
                continue;
            }
        }
        return null;
    }

    // Method apabila ada tambahan masalah di IRS
    public void addMasalahIRS(String masalah) 
    {
        String[] temp = new String[this.masalahIRS.length + 1];
        for (int i = 0; i < this.masalahIRS.length; i++) 
        {
            temp[i] = this.masalahIRS[i];
        }
        for (int i = 0; i < temp.length+1; i++) 
        {
            if (temp[i] == null) 
            {
                temp[i] = masalah;
                this.masalahIRS = temp;
            }
            if (this.masalahIRS[i].equals(masalah)) 
            {
                return;
            }
        }
    }

    // Method pengecekan masalah di IRS
    public void cekIRS(){
        this.masalahIRS = new String[1];
        for (int i = 0; i < 10; i++) 
        {
            if (this.mataKuliah[i] != null) 
            {
                if (this.totalSKS > 24) 
                {
                    addMasalahIRS(String.format("SKS yang Anda ambil lebih dari 24"));
                }
                if (this.jurusan.equals("Ilmu Komputer") 
                    && this.mataKuliah[i].getKode().equals("SI")) 
                {
                    addMasalahIRS(String.format("Mata Kuliah %s tidak dapat diambil jurusan IK", this.mataKuliah[i].getNama()));
                }
                else if (this.jurusan.equals("Sistem Informasi") 
                         && this.mataKuliah[i].getKode().equals("IK")) 
                {
                    addMasalahIRS(String.format("Mata Kuliah %s tidak dapat diambil jurusan SI", this.mataKuliah[i].getNama()));
                }
            }
        }
    }

    public String toString() {
        return this.nama;
    }
}