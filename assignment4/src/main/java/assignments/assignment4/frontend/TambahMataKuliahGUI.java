package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{

    private JTextField nama;
	private JTextField namaMatkul;
	private JTextField sks;
	private JTextField kapasitas;
	private JPanel contentPane;

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
		// Panel
        contentPane = new JPanel();
		contentPane.setBackground(new Color(47, 79, 79));
		frame.setBounds(350, 90, 849, 693);
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Label
		JLabel lblTitle = new JLabel("Tambah Mata Kuliah");
		lblTitle.setForeground(new Color(102, 0, 51));
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblTitle.setBounds(276, 111, 273, 26);
		contentPane.add(lblTitle);
		
		JLabel lblNama = new JLabel("Kode Mata Kuliah:");
		lblNama.setForeground(new Color(0, 0, 0));
		lblNama.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblNama.setBounds(103, 214, 142, 20);
		contentPane.add(lblNama);
		
		JLabel lblNamaMatkul = new JLabel("Nama Mata Kuliah:");
		lblNamaMatkul.setForeground(new Color(0, 0, 0));
		lblNamaMatkul.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblNamaMatkul.setBounds(98, 296, 147, 20);
		contentPane.add(lblNamaMatkul);
		
		JLabel lblSKS = new JLabel("SKS:");
		lblSKS.setForeground(new Color(0, 0, 0));
		lblSKS.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblSKS.setBounds(211, 376, 34, 20);
		contentPane.add(lblSKS);
		
		JLabel lblKapasitas = new JLabel("Kapasitas:");
		lblKapasitas.setForeground(new Color(0, 0, 0));
		lblKapasitas.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblKapasitas.setBounds(168, 452, 77, 20);
		contentPane.add(lblKapasitas);
		
		// Text Field
		nama = new JTextField();
		nama.setBackground(new Color(248, 248, 255));
		nama.setFont(new Font("Microsoft New Tai Lue", Font.PLAIN, 20));
		nama.setColumns(10);
		nama.setBounds(255, 207, 327, 35);
		contentPane.add(nama);
		
		namaMatkul = new JTextField();
		namaMatkul.setBackground(new Color(248, 248, 255));
		namaMatkul.setFont(new Font("Microsoft New Tai Lue", Font.PLAIN, 20));
		namaMatkul.setColumns(10);
		namaMatkul.setBounds(255, 289, 327, 35);
		contentPane.add(namaMatkul);
		
		sks = new JTextField();
		sks.setBackground(new Color(248, 248, 255));
		sks.setFont(new Font("Microsoft New Tai Lue", Font.PLAIN, 20));
		sks.setColumns(10);
		sks.setBounds(255, 369, 327, 35);
		contentPane.add(sks);
		
		kapasitas = new JTextField();
		kapasitas.setBackground(new Color(248, 248, 255));
		kapasitas.setFont(new Font("Microsoft New Tai Lue", Font.PLAIN, 20));
		kapasitas.setColumns(10);
		kapasitas.setBounds(255, 445, 327, 35);
		contentPane.add(kapasitas);

		// Button
		JButton tambah = new JButton("Tambahkan");
		tambah.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (nama.getText().equals("") || namaMatkul.getText().equals("") || sks.getText().equals("") || kapasitas.getText().equals("") ) {
				  	// Apabila ada field yang kosong
				  	JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
				}
				else {
				  	MataKuliah mataKuliah = new MataKuliah(nama.getText(), namaMatkul.getText(), Integer.parseInt(sks.getText()), Integer.parseInt(kapasitas.getText()));
				  	daftarMataKuliah.add(mataKuliah);
				  	JOptionPane.showMessageDialog(frame, String.format("Mata Kuliah %s berhasil ditambahkan", mataKuliah.getNama())); 
				}
			}
		});
		tambah.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		tambah.setForeground(new Color(47, 79, 79));
		tambah.setBackground(new Color(211, 211, 211));
		tambah.setBounds(302, 546, 227, 35);
		contentPane.add(tambah);
		
		JButton kembali = new JButton("Kembali");
		kembali.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().removeAll();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
			}
		});
		kembali.setForeground(new Color(47, 79, 79));
		kembali.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		kembali.setBackground(new Color(211, 211, 211));
		kembali.setBounds(331, 600, 182, 35);
		contentPane.add(kembali);
    }
}