package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {


    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
		
		// Label
		JLabel lblTitle = new JLabel("Detail Ringkasan Mata Kuliah");
		lblTitle.setForeground(new Color(102, 0, 51));
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblTitle.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel lblNama = new JLabel();
		lblNama.setText("Nama Mata Kuliah:", mataKuliah.getNama());
		lblNama.setForeground(Color.BLACK);
		lblNama.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblNama.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel lblKode = new JLabel();
		lblKode.setText("Kode:", mataKuliah.getKode());
		lblKode.setForeground(Color.BLACK);
		lblKode.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblKode.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel lblSks = new JLabel();
		lblSks.setText("SKS:", mataKuliah.getSKS());
		lblSks.setForeground(Color.BLACK);
		lblSks.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblSks.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel lblJumlahMahasiswa = new JLabel();
		lblKode.setText("Jumlah Mahasiswa:", mataKuliah.getJumlahMahasiswa());
		lblJumlahMahasiswa.setForeground(Color.BLACK);
		lblJumlahMahasiswa.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblJumlahMahasiswa.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel lblKapasitas = new JLabel();
		lblKapasitas.setText("Kapasitas:", mataKuliah.getKapasitas());
		lblKapasitas.setForeground(Color.BLACK);
		lblKapasitas.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblKapasitas.setHorizontalAlignment(JLabel.CENTER);
		
		JLabel lblDaftarmahasiswa = new JLabel();
		lblDaftarmahasiswa.setText("Daftar Mahasiswa:");
		lblDaftarmahasiswa.setForeground(Color.BLACK);
		lblDaftarmahasiswa.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblDaftarmahasiswa.setHorizontalAlignment(JLabel.CENTER);

		JLabel lblKosong = new JLabel("Belum ada mahasiswa yang mengambil Mata Kuliah ini");
		lblTitle.setHorizontalAlignment(JLabel.CENTER);

		// Label mahasiswa
		Mahasiswa[] mahasiswa = mataKuliah.getDaftarMahasiswa();
        JLabel[] lblMahasiswa = new JLabel[mataKuliah.getJumlahMahasiswa()];
        for (int i=0; i<mataKuliah.getJumlahMahasiswa(); i++) 
		{
            lblMahasiswa[i] = new JLabel();
            lblMahasiswa[i].setText(i+1 + ". " + mahasiswa[i]);
            lblMahasiswa[i].setHorizontalAlignment(JLabel.CENTER);
        }

		// Button
		JButton selesai = new JButton("Selesai");
		selesai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().removeAll();
				new HomeGUI().setVisible(true);
			}
		});
		selesai.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		selesai.setForeground(new Color(47, 79, 79));
		selesai.setBackground(new Color(211, 211, 211));
		selesai.setBounds(366, 648, 119, 35);
		contentPane.add(selesai);

		// Panel
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(15, 1));
		contentPane.setBackground(new Color(47, 79, 79));
        contentPane.add(Box.createHorizontalStrut(30));
        // Meletakkan komponen-komponen user interface pada panel
        contentPane.add(lblTitle);
        contentPane.add(lblNama);
        contentPane.add(lblKode);
        contentPane.add(lblSks);
        contentPane.add(lblJumlahMahasiswa);
		contentPane.add(lblKapasitas);
		contentPane.add(lblDaftarmahasiswa);

		// Apabila mahasiswa masih 0
        if (mataKuliah.getJumlahMahasiswa() == 0) 
		{ 
            contentPane.add(lblKosong);
        }

		// Label nama mahasiswa
        else {
            for (int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++) 
			{
                contentPane.add(lblMahasiswa[i]);
            }
        }

        contentPane.add(selesai);


        frame.add(contentPane);
        frame.setVisible(true);
    }
}