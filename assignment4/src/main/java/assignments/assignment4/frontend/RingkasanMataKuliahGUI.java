package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {

    private JPanel contentPane;
	private JComboBox namaMatkul;

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

		// Algoritma sorting untuk mengurutkan mata kuliah
		boolean flag2 = false;
		while (!flag2) {
			flag2 = true;
			for (int i = 0; i < daftarMataKuliah.size()-1; i++) 
			{
				MataKuliah matkul = daftarMataKuliah.get(i);
				MataKuliah greaterMatkul = daftarMataKuliah.get(i+1);
				if (matkul.getNama().toLowerCase().compareTo(greaterMatkul.getNama().toLowerCase()) > 0) {
					flag2 = false;
					daftarMataKuliah.set(i+1, matkul);
					daftarMataKuliah.set(i, greaterMatkul);
				}
			}
		}

		MataKuliah[] sortNamaMatkul = new MataKuliah[daftarMataKuliah.size()];
		daftarMataKuliah.toArray(sortNamaMatkul);

		// Panel
        contentPane = new JPanel();
		contentPane.setBackground(new Color(47, 79, 79));
		frame.setBounds(505, 90, 849, 693);
		contentPane.setLayout(null);
		frame.setContentPane(contentPane);
		
		// Label
		JLabel lblTitle = new JLabel("Ringkasan Mata Kuliah");
		lblTitle.setForeground(new Color(102, 0, 51));
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblTitle.setBounds(255, 110, 311, 34);
		contentPane.add(lblTitle);
		
		JLabel lblNamaMatkul = new JLabel("Pilih Nama Matkul:");
		lblNamaMatkul.setForeground(new Color(0, 0, 0));
		lblNamaMatkul.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblNamaMatkul.setBounds(97, 209, 148, 20);
		contentPane.add(lblNamaMatkul);
		
		// Combo Box
		JComboBox namaMatkul = new JComboBox<>(sortNamaMatkul);
		namaMatkul.setBackground(new Color(255, 255, 255));
		namaMatkul.setBounds(255, 204, 325, 34);
		contentPane.add(namaMatkul);
		
		// Button 
		JButton lihat = new JButton("Lihat");
		lihat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(namaMatkul.getSelectedItem()==null)
				{
					JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
				}
				else
				{
					String nama = (String) namaMatkul.getSelectedItem();
					MataKuliah objekMataKuliah = getMataKuliah(nama);
					frame.getContentPane().removeAll();
					new DetailRingkasanMataKuliahGUI(frame, objekMataKuliah, daftarMahasiswa, daftarMataKuliah);
				}
			}
		});
		lihat.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lihat.setForeground(new Color(47, 79, 79));
		lihat.setBackground(new Color(211, 211, 211));
		lihat.setBounds(302, 429, 227, 35);
		contentPane.add(lihat);
		
		JButton kembali = new JButton("Kembali");
		kembali.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().removeAll();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
			}
		});
		kembali.setForeground(new Color(47, 79, 79));
		kembali.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		kembali.setBackground(new Color(211, 211, 211));
		kembali.setBounds(326, 485, 182, 35);
		contentPane.add(kembali);
        
    }

	// Mengambil objek mata kuliah dari daftar mata kuliah
    private MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
}
