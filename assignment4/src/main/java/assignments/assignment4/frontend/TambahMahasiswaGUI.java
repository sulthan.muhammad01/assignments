package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{

    private JTextField nama;
	private JTextField npm;
    private JPanel contentPane;

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
       
        contentPane = new JPanel();
		contentPane.setBackground(new Color(47, 79, 79));
		frame.setBounds(350, 90, 849, 693);
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Title
		JLabel lblTitle = new JLabel("Tambah Mahasiswa");
		lblTitle.setForeground(new Color(102, 0, 51));
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblTitle.setBounds(288, 111, 261, 26);
		contentPane.add(lblTitle);
		
		// Nama Mahasiswa
		JLabel lblNama = new JLabel("Nama:");
		lblNama.setForeground(new Color(0, 0, 0));
		lblNama.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblNama.setBounds(195, 214, 50, 20);
		contentPane.add(lblNama);
		
		nama = new JTextField();
		nama.setBackground(new Color(248, 248, 255));
		nama.setFont(new Font("Microsoft New Tai Lue", Font.PLAIN, 20));
		nama.setColumns(10);
		nama.setBounds(255, 207, 327, 35);
		contentPane.add(nama);
		
		// NPM Mahasiswa
		JLabel lblNPM = new JLabel("NPM:");
		lblNPM.setForeground(new Color(0, 0, 0));
		lblNPM.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblNPM.setBounds(202, 296, 43, 20);
		contentPane.add(lblNPM);
		
		npm = new JTextField();
		npm.setBackground(new Color(248, 248, 255));
		npm.setFont(new Font("Microsoft New Tai Lue", Font.PLAIN, 20));
		npm.setColumns(10);
		npm.setBounds(255, 289, 327, 35);
		contentPane.add(npm);
		
		// Tombol tambah dan kembali
		JButton tambah = new JButton("Tambahkan");
		tambah.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (nama.getText().equals("") || npm.getText().equals("") ) {
				  	// Apabila ada field yang kosong
					JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
				}
				else 
				{
				  	Mahasiswa mahasiswa = new Mahasiswa(nama.getText(), Long.parseLong(npm.getText()));
				  	daftarMahasiswa.add(mahasiswa);
				  	JOptionPane.showMessageDialog(frame, String.format("Mahasiswa %d-%s berhasil ditambahkan", mahasiswa.getNpm(), mahasiswa.getNama())); 
				}	
			}
		});
		tambah.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		tambah.setForeground(new Color(0, 0, 0));
		tambah.setBackground(new Color(211, 211, 211));
		tambah.setBounds(302, 429, 227, 35);
		contentPane.add(tambah);
		
		JButton kembali = new JButton("Kembali");
		kembali.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().removeAll();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
			}
		});
		kembali.setForeground(new Color(0, 0, 0));
		kembali.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		kembali.setBackground(new Color(211, 211, 211));
		kembali.setBounds(326, 485, 182, 35);
		contentPane.add(kembali);
	}
}