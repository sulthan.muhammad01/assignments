package assignments.assignment4.frontend;

import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    //private Image logo = new ImageIcon(HomeGUI.class.getResource("res/logo.png")).getImage().getScaledInstance(1000, 1000, Image.SCALE_SMOOTH);;
	//private Image logo2 = new ImageIcon(HomeGUI.class.getResource("res/book.png")).getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH);
	//private Image tambahMahasiswa = new ImageIcon(HomeGUI.class.getResource("res/contacts.png")).getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH);
	//private Image tambahMatkul = new ImageIcon(HomeGUI.class.getResource("res/uncheck.png")).getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH);
	//private Image tambahIRS = new ImageIcon(HomeGUI.class.getResource("res/plus.png")).getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH);
	//private Image hapusIRS = new ImageIcon(HomeGUI.class.getResource("res/cancel.png")).getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH);
	//private Image ringkasanMahasiswa = new ImageIcon(HomeGUI.class.getResource("res/search2.png")).getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH);
	//private Image ringkasanMatkul = new ImageIcon(HomeGUI.class.getResource("res/browse.png")).getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH);
	private JPanel contentPane;

    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(170, 50, 1200, 750);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(47, 79, 79));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Panel
		JPanel panel = new JPanel();
		panel.setBackground(new Color(128, 128, 0));
		panel.setBounds(0, 28, 317, 722);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel homePane = new JPanel();
		homePane.setBounds(327, 29, 863, 711);
		contentPane.add(homePane);
		homePane.setBackground(new Color(47, 79, 79));
		homePane.setLayout(null);
		
		JPanel tambahMahasiswaPanel = new JPanel();
		tambahMahasiswaPanel.addMouseListener(new PanelButtonMouseAdapter(tambahMahasiswaPanel) {
			@Override
			public void mouseClicked(MouseEvent e) {
				homePane.setVisible(false);
				new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
			}
		});
		tambahMahasiswaPanel.setBackground(new Color(128, 128, 0));
		tambahMahasiswaPanel.setBounds(0, 216, 317, 50);
		panel.add(tambahMahasiswaPanel);
		tambahMahasiswaPanel.setLayout(null);
		
		JPanel tambahMatkulPanel = new JPanel();
		tambahMatkulPanel.addMouseListener(new PanelButtonMouseAdapter(tambahMatkulPanel) {
			@Override
			public void mouseClicked(MouseEvent e) {
				homePane.setVisible(false);
				new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
			}
		});
		tambahMatkulPanel.setBackground(new Color(128, 128, 0));
		tambahMatkulPanel.setBounds(0, 269, 317, 50);
		panel.add(tambahMatkulPanel);
		tambahMatkulPanel.setLayout(null);
		
		JPanel tambahIRSPanel = new JPanel();
		tambahIRSPanel.addMouseListener(new PanelButtonMouseAdapter(tambahIRSPanel) {
			@Override
			public void mouseClicked(MouseEvent e) {
				homePane.setVisible(false);
				new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
			}
		});
		tambahIRSPanel.setBackground(new Color(128, 128, 0));
		tambahIRSPanel.setBounds(0, 322, 317, 50);
		panel.add(tambahIRSPanel);
		tambahIRSPanel.setLayout(null);
				
		JPanel hapusIRSPanel = new JPanel();
		hapusIRSPanel.addMouseListener(new PanelButtonMouseAdapter(hapusIRSPanel) {
			@Override
			public void mouseClicked(MouseEvent e) {
				homePane.setVisible(false);
				new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
			}
		});
		hapusIRSPanel.setBackground(new Color(128, 128, 0));
		hapusIRSPanel.setBounds(0, 375, 317, 50);
		panel.add(hapusIRSPanel);
		hapusIRSPanel.setLayout(null);
		
		JPanel detailMahasiswaPanel = new JPanel();
		detailMahasiswaPanel.addMouseListener(new PanelButtonMouseAdapter(detailMahasiswaPanel) {
			@Override
			public void mouseClicked(MouseEvent e) {
				homePane.setVisible(false);
				new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
			}
		});
		detailMahasiswaPanel.setBackground(new Color(128, 128, 0));
		detailMahasiswaPanel.setBounds(0, 428, 317, 50);
		panel.add(detailMahasiswaPanel);
		detailMahasiswaPanel.setLayout(null);
		
		JPanel detailMatkulPanel = new JPanel();
		detailMatkulPanel.addMouseListener(new PanelButtonMouseAdapter(detailMatkulPanel) {
			@Override
			public void mouseClicked(MouseEvent e) {
				homePane.setVisible(false);
				new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
			}
		});
		detailMatkulPanel.setBackground(new Color(128, 128, 0));
		detailMatkulPanel.setBounds(0, 481, 317, 50);
		panel.add(detailMatkulPanel);
		detailMatkulPanel.setLayout(null);

		JPanel headTittle = new JPanel();
		headTittle.setBackground(new Color(189, 183, 107));
		headTittle.setBounds(0, 0, 1200, 28);
		contentPane.add(headTittle);
		headTittle.setLayout(null);
		
		// Logo
		JLabel lblLogo = new JLabel("");
		lblLogo.setBounds(-56, 49, 703, 634);
		homePane.add(lblLogo);
		//lblLogo.setIcon(new ImageIcon(logo));
		
		JLabel lblTambahMahasiswa = new JLabel("");
		lblTambahMahasiswa.setBounds(35, 0, 50, 50);
		tambahMahasiswaPanel.add(lblTambahMahasiswa);
		//lblTambahMahasiswa.setIcon(new ImageIcon(tambahMahasiswa));
		
		JLabel lblTambahMatkul = new JLabel("");
		lblTambahMatkul.setBounds(35, 0, 50, 50);
		tambahMatkulPanel.add(lblTambahMatkul);
		//lblTambahMatkul.setIcon(new ImageIcon(tambahMatkul));
		
		JLabel lblTambahIRS = new JLabel("");
		lblTambahIRS.setBounds(35, 0, 50, 50);
		tambahIRSPanel.add(lblTambahIRS);
		//lblTambahIRS.setIcon(new ImageIcon(tambahIRS));
		
		JLabel lblRingkasanMahasiswa = new JLabel("");
		lblRingkasanMahasiswa.setBounds(35, 0, 50, 50);
		detailMahasiswaPanel.add(lblRingkasanMahasiswa);
		//lblRingkasanMahasiswa.setIcon(new ImageIcon(ringkasanMahasiswa));
		
		JLabel lblRingkasanMataKuliah = new JLabel("");
		lblRingkasanMataKuliah.setBounds(35, 0, 50, 50);
		detailMatkulPanel.add(lblRingkasanMataKuliah);
		//lblRingkasanMataKuliah.setIcon(new ImageIcon(ringkasanMatkul));

		JLabel lblLogo2 = new JLabel("");
		lblLogo2.setBounds(110, 31, 100, 100);
		panel.add(lblLogo2);
		lblLogo2.setForeground(new Color(205, 133, 63));
		//lblLogo2.setIcon(new ImageIcon(logo2));

		JLabel lblHapusIRS = new JLabel("");
		lblHapusIRS.setBounds(35, 0, 50, 50);
		hapusIRSPanel.add(lblHapusIRS);
		//lblHapusIRS.setIcon(new ImageIcon(hapusIRS));
		
		// label
		JLabel lblLihatDetailMatkul = new JLabel("Lihat Detail Mata Kuliah");
		lblLihatDetailMatkul.setForeground(new Color(0, 0, 0));
		lblLihatDetailMatkul.setBounds(109, 10, 186, 30);
		detailMatkulPanel.add(lblLihatDetailMatkul);
		lblLihatDetailMatkul.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		
		JLabel lblNewLabel = new JLabel("Tambah Mahasiswa");
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setBounds(110, 10, 160, 30);
		tambahMahasiswaPanel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		
		JLabel lblTambahMataKuliah = new JLabel("Tambah Mata Kuliah");
		lblTambahMataKuliah.setForeground(new Color(0, 0, 0));
		lblTambahMataKuliah.setBounds(110, 10, 175, 30);
		tambahMatkulPanel.add(lblTambahMataKuliah);
		lblTambahMataKuliah.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		
		JLabel lblTambahIrs = new JLabel("Tambah IRS");
		lblTambahIrs.setForeground(new Color(0, 0, 0));
		lblTambahIrs.setBounds(110, 10, 115, 28);
		tambahIRSPanel.add(lblTambahIrs);
		lblTambahIrs.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		
		JLabel lblLihatDetailMahasiswa = new JLabel("Lihat Detail Mahasiswa");
		lblLihatDetailMahasiswa.setForeground(new Color(0, 0, 0));
		lblLihatDetailMahasiswa.setBounds(110, 10, 177, 30);
		detailMahasiswaPanel.add(lblLihatDetailMahasiswa);
		lblLihatDetailMahasiswa.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		
		JLabel lblHapusIrs = new JLabel("Hapus IRS");
		lblHapusIrs.setForeground(new Color(0, 0, 0));
		lblHapusIrs.setBounds(110, 10, 98, 30);
		hapusIRSPanel.add(lblHapusIrs);
		lblHapusIrs.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		
		JLabel lblWelcomeTo = new JLabel("Welcome To");
		lblWelcomeTo.setForeground(new Color(0, 0, 0));
		lblWelcomeTo.setBounds(80, 100, 142, 40);
		homePane.add(lblWelcomeTo);
		lblWelcomeTo.setFont(new Font("Serif", Font.BOLD, 26));
		
		JLabel lblTittle = new JLabel("Administrator - Sistem Akademik");
		lblTittle.setBounds(500, 7, 248, 19);
		headTittle.add(lblTittle);
		lblTittle.setFont(new Font("Microsoft Himalaya", Font.PLAIN, 24));

		JLabel lblExit = new JLabel("x");
		lblExit.setBounds(1172, 0, 18, 22);
		headTittle.add(lblExit);
		lblExit.setFont(new Font("Tahoma", Font.PLAIN, 28));
		
		// Mouse listener untuk tombol X
		lblExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(JOptionPane.showConfirmDialog(null, "Are you sure to close the application", "confirmation", JOptionPane.YES_NO_OPTION)==0) {
					System.exit(0);
				}
			}
			@Override
			public void mouseEntered(MouseEvent arg0) {
				lblExit.setForeground(Color.RED);
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				lblExit.setForeground(Color.BLACK);
			}
		});
	}

	// Class untuk mouse adapter
	private class PanelButtonMouseAdapter extends MouseAdapter{
		JPanel panel;
		
		public PanelButtonMouseAdapter(JPanel panel) {
			this.panel = panel;
		}
		@Override
		public void mouseEntered(MouseEvent e) {
			panel.setBackground(SystemColor.windowBorder);
		}	
		@Override
		public void mouseExited(MouseEvent e) {
			panel.setBackground(new Color(128, 128, 0));
		}	
		@Override
		public void mousePressed(MouseEvent e) {
			panel.setBackground(new Color(255, 250, 250));
		}	
		@Override
		public void mouseReleased(MouseEvent e) {
			panel.setBackground(new Color(128, 128, 0));
		}	
	}
}