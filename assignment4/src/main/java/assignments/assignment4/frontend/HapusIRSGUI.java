package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HapusIRSGUI {

    private JPanel contentPane;

    public HapusIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

		boolean flag = false; 
		while (!flag) 
		{
			flag = true;
			for (int i=0; i<daftarMahasiswa.size()-1; i++) 
			{
				Mahasiswa mahasiswa1 = daftarMahasiswa.get(i);
				Mahasiswa mahasiswa2 = daftarMahasiswa.get(i+1);
				if (mahasiswa2.getNpm() < mahasiswa1.getNpm()) 
				{
					flag = false;
					daftarMahasiswa.set(i, mahasiswa2);
					daftarMahasiswa.set(i+1, mahasiswa1);
				}
			}
		}

		// Sorting mata kuliah 
		boolean flag2 = false;
		while (!flag2) 
		{
			flag2 = true;
			for (int i = 0; i < daftarMataKuliah.size()-1; i++) 
			{
				MataKuliah matkul = daftarMataKuliah.get(i);
				MataKuliah greaterMatkul = daftarMataKuliah.get(i+1);
				if (matkul.getNama().toLowerCase().compareTo(greaterMatkul.getNama().toLowerCase()) > 0) 
				{
					flag2 = false;
					daftarMataKuliah.set(i+1, matkul);
					daftarMataKuliah.set(i, greaterMatkul);
				}
			}
		}

		// Menambahkannya ke dalam list baru
		Mahasiswa[] sortNPM = new Mahasiswa[daftarMahasiswa.size()];
		daftarMahasiswa.toArray(sortNPM);

		MataKuliah[] sortNamaMatkul = new MataKuliah[daftarMataKuliah.size()];
		daftarMataKuliah.toArray(sortNamaMatkul);

		// Panel 
        contentPane = new JPanel();
		contentPane.setBackground(new Color(47, 79, 79));
		frame.setBounds(350, 90, 849, 693);
		contentPane.setLayout(null);
		frame.setContentPane(contentPane);
		
		// Label
		JLabel lblTitle = new JLabel("Hapus IRS");
		lblTitle.setForeground(new Color(102, 0, 51));
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblTitle.setBounds(341, 111, 140, 34);
		contentPane.add(lblTitle);
		
		JLabel lblNPM = new JLabel("Pilih NPM:");
		lblNPM.setForeground(new Color(0, 0, 0));
		lblNPM.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblNPM.setBounds(163, 209, 82, 20);
		contentPane.add(lblNPM);
		
		JLabel lblNamaMatkul = new JLabel("Pilih Nama Matkul:");
		lblNamaMatkul.setForeground(new Color(0, 0, 0));
		lblNamaMatkul.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblNamaMatkul.setBounds(97, 296, 148, 20);
		contentPane.add(lblNamaMatkul);
		
		// Combo Box
		JComboBox npm = new JComboBox<>(sortNPM);
		npm.setBackground(new Color(255, 255, 255));
		npm.setBounds(255, 204, 325, 34);
		contentPane.add(npm);
		
		JComboBox namaMatkul = new JComboBox(sortNamaMatkul);
		namaMatkul.setBackground(new Color(255, 255, 255));
		namaMatkul.setBounds(255, 291, 325, 34);
		contentPane.add(namaMatkul);
		
		// Button
		JButton hapus = new JButton("Hapus");
		hapus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
                Mahasiswa mahasiswa = (Mahasiswa) npm.getSelectedItem();
                MataKuliah mataKuliah = (MataKuliah) namaMatkul.getSelectedItem();
                if (npm.getSelectedItem() == null || namaMatkul.getSelectedItem() == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }
                else {
                    JOptionPane.showMessageDialog(frame, mahasiswa.dropMatkul(mataKuliah));
                }
            }
        });
		hapus.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		hapus.setForeground(new Color(47, 79, 79));
		hapus.setBackground(new Color(211, 211, 211));
		hapus.setBounds(302, 429, 227, 35);
		contentPane.add(hapus);
		
		JButton kembali = new JButton("Kembali");
		kembali.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().removeAll();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
			}
		});
		kembali.setForeground(new Color(47, 79, 79));
		kembali.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		kembali.setBackground(new Color(211, 211, 211));
		kembali.setBounds(326, 485, 182, 35);
		contentPane.add(kembali);
    }
}