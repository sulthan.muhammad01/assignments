package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    private JPanel contentPane;
	private JComboBox npm;

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

		// Algoritma sorting untuk mengurutkan NPM
		boolean flag = false; 
		while (!flag) {
			flag = true;
			for (int i=0; i<daftarMahasiswa.size()-1; i++) 
			{
				Mahasiswa mahasiswa1 = daftarMahasiswa.get(i);
				Mahasiswa mahasiswa2 = daftarMahasiswa.get(i+1);
				if (mahasiswa1.getNpm() < mahasiswa2.getNpm()) {
					flag = false;
					daftarMahasiswa.set(i, mahasiswa2);
					daftarMahasiswa.set(i+1, mahasiswa1);
				}
			}
		}

		Mahasiswa[] sortNPM = new Mahasiswa[daftarMahasiswa.size()];
		daftarMahasiswa.toArray(sortNPM);

		// Panel
        contentPane = new JPanel();
		contentPane.setBackground(new Color(47, 79, 79));
		frame.setBounds(350, 90, 849, 693);
		contentPane.setLayout(null);
		frame.setContentPane(contentPane);
		
		// Label
		JLabel lblTitle = new JLabel("Ringkasan Mahasiswa");
		lblTitle.setForeground(new Color(102, 0, 51));
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblTitle.setBounds(272, 111, 294, 34);
		contentPane.add(lblTitle);
		
		JLabel lblNPM = new JLabel("Pilih NPM:");
		lblNPM.setForeground(new Color(0, 0, 0));
		lblNPM.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lblNPM.setBounds(163, 209, 82, 20);
		contentPane.add(lblNPM);
		
		// Combo Box
		JComboBox npm = new JComboBox<Long>(sortNPM);
		npm.setBackground(new Color(255, 255, 255));
		npm.setBounds(255, 204, 325, 34);
		contentPane.add(npm);
		
		// Button
		JButton lihat = new JButton("Lihat");
		lihat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				if(npm.getSelectedItem()==null)
				{
					JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
				}
				else
				{
					Long npmMahasiswa = (Long) npm.getSelectedItem();
					Mahasiswa objekMahasiswa = getMahasiswa(npmMahasiswa);
					frame.getContentPane().removeAll();
					new DetailRingkasanMahasiswaGUI(frame, objekMahasiswa, daftarMahasiswa, daftarMataKuliah);
				}
			}
		});
		lihat.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		lihat.setForeground(new Color(47, 79, 79));
		lihat.setBackground(new Color(211, 211, 211));
		lihat.setBounds(302, 429, 227, 35);
		contentPane.add(lihat);
		
		JButton kembali = new JButton("Kembali");
		kembali.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().removeAll();
				new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
			}
		});
		kembali.setForeground(new Color(47, 79, 79));
		kembali.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		kembali.setBackground(new Color(211, 211, 211));
		kembali.setBounds(326, 485, 182, 35);
		contentPane.add(kembali);
    }

	// Untuk mengambil objek mahasiswa dari daftar mahasiswa
    private Mahasiswa getMahasiswa(long npm) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}
