package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class SistemAkademik {

    
    public static void main(String[] args) { 
        new SistemAkademikGUI();
    }
}

class SistemAkademikGUI extends JFrame{
    private static ArrayList<Mahasiswa> daftarMahasiswa = new ArrayList<Mahasiswa>();
    private static ArrayList<MataKuliah> daftarMataKuliah = new ArrayList<MataKuliah>();

    public SistemAkademikGUI(){

        // Membuat Frame
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1200, 750);
        // TODO: Tambahkan hal-hal lain yang diperlukan
        frame.setTitle("Administrator - Sistem Akademik");
        frame.setLocationRelativeTo(null);
        frame.setUndecorated(true);

        new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
        frame.setVisible(true);


    }
}