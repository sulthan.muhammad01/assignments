package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {

    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        JLabel lblTitle = new JLabel("Detail Ringkasan Mahasiswa");
        lblTitle.setText("Detail Ringkasan Mahasiswa");
        lblTitle.setForeground(new Color(102, 0, 51));
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 26));
        lblTitle.setHorizontalAlignment(JLabel.CENTER);

        JLabel lblNama = new JLabel();
        lblNama.setText("Nama: " + mahasiswa.getNama());
		lblNama.setForeground(Color.BLACK);
		lblNama.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
        lblNama.setHorizontalAlignment(JLabel.CENTER);

        JLabel lblNPM = new JLabel();
        lblNPM.setText("NPM: " + mahasiswa.getNpm());
        lblNPM.setForeground(Color.BLACK);
		lblNPM.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
        lblNPM.setHorizontalAlignment(JLabel.CENTER);

        JLabel lblJurusan = new JLabel();
        lblJurusan.setText("Jurusan: " + mahasiswa.getJurusan());
        lblJurusan.setForeground(Color.BLACK);
		lblJurusan.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
        lblJurusan.setHorizontalAlignment(JLabel.CENTER);

        JLabel lblDaftarMatkul = new JLabel();
        lblDaftarMatkul.setText("Daftar Mata Kuliah:");
        lblDaftarMatkul.setForeground(Color.BLACK);
		lblDaftarMatkul.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
        lblDaftarMatkul.setHorizontalAlignment(JLabel.CENTER);

        JLabel lblTotalSKS = new JLabel();
        lblTotalSKS.setText("Total SKS: " + mahasiswa.getTotalSKS());
        lblTotalSKS.setForeground(Color.BLACK);
		lblTotalSKS.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
        lblTotalSKS.setHorizontalAlignment(JLabel.CENTER);

        JLabel lblHasilIRS = new JLabel();
        lblHasilIRS.setText("Hasil Pengecekan IRS:");
        lblHasilIRS.setForeground(Color.BLACK);
		lblHasilIRS.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
        lblHasilIRS.setHorizontalAlignment(JLabel.CENTER);

        JLabel lblMatkulKosong = new JLabel();
        lblMatkulKosong.setText("Belum ada mata kuliah yang diambil.");
        lblMatkulKosong.setHorizontalAlignment(JLabel.CENTER);

        JLabel lblIRSAman = new JLabel();
        lblIRSAman.setText("IRS tidak bermasalah.");
        lblIRSAman.setHorizontalAlignment(JLabel.CENTER);

        // Label matkul dan masalah IRS
		MataKuliah[] matakuliah = mahasiswa.getMataKuliah();
        JLabel[] lblMatkul = new JLabel[mahasiswa.getBanyakMatkul()];
        for (int i = 0; i < mahasiswa.getBanyakMatkul(); i++) 
		{
            lblMatkul[i] = new JLabel();
            lblMatkul[i].setText(i+1 + ". " + matakuliah[i]);
            lblMatkul[i].setHorizontalAlignment(JLabel.CENTER);
        }

        mahasiswa.cekIRS();
        String[] masalah = mahasiswa.getMasalahIRS();
        JLabel[] lblMasalah = new JLabel[mahasiswa.getBanyakMasalahIRS()];
        for (int i = 0; i < mahasiswa.getBanyakMasalahIRS(); i++) 
		{
            lblMasalah[i] = new JLabel();
            lblMasalah[i].setText(i+1 + ". " + masalah[i]);
            lblMasalah[i].setHorizontalAlignment(JLabel.CENTER);
        }

        // Button
        JButton selesai = new JButton();
        selesai.setText("Selesai");
        selesai.setFont(new Font("Microsoft JhengHei UI", Font.BOLD, 16));
		selesai.setForeground(new Color(47, 79, 79));
		selesai.setBackground(new Color(211, 211, 211));
        selesai.setHorizontalAlignment(JLabel.CENTER);
        selesai.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        // Panel
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(15, 1));
        contentPane.add(Box.createHorizontalStrut(30));
        // Meletakkan komponen-komponen user interface pada panel
        contentPane.add(lblTitle);
        contentPane.add(lblNama);
        contentPane.add(lblNPM);
        contentPane.add(lblJurusan);
        contentPane.add(lblDaftarMatkul);

		// Apabila matkul masih 0
        if (mahasiswa.getBanyakMatkul() == 0) 
		{ 
            contentPane.add(lblMatkulKosong);
        }

		// Label nama mata kuliah
        else {
            for (int i = 0; i < mahasiswa.getBanyakMatkul(); i++) 
			{
                contentPane.add(lblMatkul[i]);
            }
        }

        contentPane.add(lblTotalSKS);
        contentPane.add(lblHasilIRS);

		// Apabila masalah IRS 0
        if (mahasiswa.getBanyakMasalahIRS() == 0) 
		{ 
            contentPane.add(lblIRSAman);
        }

		// Label masalah IRS
        else 
		{ 
            for (int i = 0; i < mahasiswa.getBanyakMasalahIRS(); i++) 
			{
                contentPane.add(lblMasalah[i]);
            }
        }

        contentPane.add(selesai);


        frame.add(contentPane);
        frame.setVisible(true);
    }
}